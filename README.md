# tailveil

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Like Tailscale but using [Veilid](https://veilid.com/)

## Demo

https://asciinema.org/a/605720

## Usage

```sh
# generate a key on each computer
tailveil --gen-key --key not_relevant_here --path not_relevant_here --user not_relevant_here > tailveil.key
```

Create a `nodes.txt` file like this (the pubkey is the first part of tailveil.key):
```
desktop,VLD0:zLpLFTRWX9nSH2A1UNZ4Wh4eLXchPLbaQbekNmgvu0U,10.44.1.1
laptop,VLD0:...,10.44.1.2
```
Each node needs the `nodes.txt` file for now.

```sh
# run
sudo tailveil --key tailveil.key --path data_directory --user my_username
```

### To try using the cachix cache

```sh
nix shell gitlab:bbigras/tailveil \
    --substituters https://tailveil.cachix.org \
    --trusted-public-keys tailveil.cachix.org-1:VKgwtnjyR3+ujBK+0yOZ/HAA+xaBKGMhfitZ4bgT61w=

# then run tailveil
```

## Todo

- [ ] mobile support
- [ ] windows support
- [ ] darwin support
- [ ] ipv6 support
- [ ] ACLs
- [ ] use the DHT
