#![forbid(unsafe_code)]

mod config;
mod tun;
mod utils;

use anyhow::{Context, Error};
use clap::Parser;
use flume::{unbounded, Receiver, Sender};
use futures_util::select;
use tracing::info;
use tracing_subscriber::EnvFilter;
use veilid_core::tools::{spawn, Arc};
use veilid_core::{
    api_startup, CryptoKind, CryptoTyped, KeyPair, PublicKey, VeilidUpdate, CRYPTO_KIND_VLD0,
};

use std::net::Ipv4Addr;
use std::path::PathBuf;

use crate::config::config_callback;
use crate::utils::{wait_for_attached, wait_for_network_start, wait_for_public_internet_ready};

const MAX_APP_MESSAGE_LEN: usize = 32768;
const CRYPTO_KIND: CryptoKind = CRYPTO_KIND_VLD0;

#[derive(Clone, Debug)]
pub struct Node {
    pub alias: String,
    pub public_key: CryptoTyped<PublicKey>,
    pub addr: Ipv4Addr,
}

fn read_nodes_file() -> Result<Vec<Node>, Error> {
    use std::io::BufRead;

    let file = std::fs::File::open("nodes.txt")?;
    let buf = std::io::BufReader::new(file);

    let mut list = vec![];

    for line in buf.lines() {
        let line = line.unwrap();
        let mut split = line.split(',');

        let alias = split.next().unwrap().to_string();
        let public_key = split.next().unwrap().parse().unwrap();
        let addr = split.next().unwrap().parse().unwrap();

        list.push(Node {
            alias,
            public_key,
            addr,
        });
    }

    Ok(list)
}

/// Like tailscale but with Veilid
#[derive(Parser, Debug)]
struct Args {
    #[arg(long)]
    key: PathBuf,

    #[arg(long)]
    path: PathBuf,

    #[arg(long)]
    gen_key: bool,

    #[arg(long)]
    user: String,

    #[arg(long, default_value = "veilid0")]
    interface: String,

    #[arg(long, default_value = "255.255.255.0")]
    netmask: Ipv4Addr,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Args::parse();

    if args.gen_key {
        let key_pair = veilid_core::Crypto::generate_keypair(CRYPTO_KIND)?;
        println!("{}", key_pair.to_string());
        return Ok(());
    }

    let default_env_filter = EnvFilter::try_from_default_env();
    let fallback_filter = EnvFilter::new("veilid_core=warn,info");
    let env_filter = default_env_filter.unwrap_or(fallback_filter);

    tracing_subscriber::fmt()
        .with_writer(std::io::stderr)
        .with_env_filter(env_filter)
        .init();

    // ---------------
    // LOAD MY KEY
    let data = std::fs::read_to_string(&args.key)
        .with_context(|| format!("can't read key file: {}", args.key.display()))?;
    let key_pair: CryptoTyped<KeyPair> = data.trim().parse().context("can't parse keypair")?;
    // ---------------

    // Read the nodes from nodes.txt
    let nodes = read_nodes_file()?;

    // Determine my IP from the nodes list using my public key
    let my_ip = nodes
        .iter()
        .find(|v| v.public_key.value == key_pair.value.key)
        .map(|v| v.addr)
        .context("can't find my IP from the nodes.txt file")?;

    // ---------------
    let (app_call_tx, app_call_rx): (
        Sender<veilid_core::VeilidAppMessage>,
        Receiver<veilid_core::VeilidAppMessage>,
    ) = unbounded();

    let (tx_from_tun, rx_from_tun): (Sender<Vec<u8>>, Receiver<Vec<u8>>) = unbounded();

    // Create TUN device
    let nodes_clone = nodes.clone();
    let tun_handle = tun::create(
        my_ip,
        args.netmask,
        app_call_rx,
        tx_from_tun,
        nodes_clone,
        &args.interface,
    )
    .await?;

    // TODO: drop privileges just after creating the TUN device
    privdrop::PrivDrop::default()
        // .chroot("/var/empty")
        .user(args.user)
        .apply()
        .context("failed to drop privileges")?;

    let (sender, receiver): (
        Sender<veilid_core::VeilidUpdate>,
        Receiver<veilid_core::VeilidUpdate>,
    ) = unbounded();

    let update_callback = Arc::new(move |change: veilid_core::VeilidUpdate| {
        if let Err(e) = sender.send(change) {
            // Don't log here, as that loops the update callback in some cases and will deadlock
            let change = e.into_inner();
            info!("error sending veilid update callback: {:?}", change);
        }
    });

    info!("veilid: connecting...");
    let api = api_startup(
        update_callback,
        Arc::new(move |key| config_callback(args.path.clone(), key_pair, key)),
    )
    .await
    .expect("startup failed");

    api.attach().await?;

    wait_for_network_start(&api).await;

    let attachment_manager = api.attachment_manager()?;
    wait_for_attached(&attachment_manager).await;

    let rc = api.routing_context();

    wait_for_public_internet_ready(&attachment_manager).await?;
    info!("veilid: READY");

    let handle_tun_ingress = spawn(async move {
        tun::handle_ingress(rc, nodes, my_ip, rx_from_tun)
            .await
            .unwrap();
        Ok::<_, Error>(())
    });

    // handle trafic from veilid
    let handle_veilid = spawn(async move {
        loop {
            select! {
                res = receiver.recv_async() => {
                    if let Ok(change) = res {
                        match change {
                            VeilidUpdate::AppMessage(app_call) => {
                                app_call_tx.send_async(app_call).await?;
                            },
                            VeilidUpdate::Shutdown => info!("VeilidUpdate::Shutdown"),
                            _ => ()
                        }
                    } else {
                        info!("update_receiver_jh, break");
                        break;
                    }
                }
            };
        }
        Ok::<_, Error>(())
    });

    tokio::select! {
        res1 = handle_veilid => res1?,
        res2 = tun_handle => res2?,
        res3 = handle_tun_ingress => res3?,
    }

    Ok(())
}
