use anyhow::{Context, Error};
use etherparse::{IpHeader, PacketHeaders};
use flume::{Receiver, Sender};
use futures_util::select;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio_tun::Tun;
use tracing::{error, info, warn};
use veilid_core::RoutingContext;

use std::future::Future;
use std::net::Ipv4Addr;

use crate::{Node, MAX_APP_MESSAGE_LEN};

#[tracing::instrument(err, skip(app_call_rx, tx, nodes))]
pub async fn create(
    addr: Ipv4Addr,
    netmask: Ipv4Addr,
    app_call_rx: Receiver<veilid_core::VeilidAppMessage>,
    tx: Sender<Vec<u8>>,
    nodes: Vec<Node>,
    interface_name: &str,
) -> Result<impl Future<Output = Result<(), Error>>, Error> {
    let tun = Tun::builder()
        .name(interface_name)
        .address(addr)
        .netmask(netmask)
        .tap(false)
        .packet_info(false)
        .up()
        .try_build()?;

    info!("tun created");

    let (mut reader, mut writer) = tokio::io::split(tun);

    let rx2 = tokio::spawn(async move {
        loop {
            let blob2 = app_call_rx.recv_async().await.unwrap();

            if let Some(sender) = blob2.sender() {
                if nodes.iter().any(|v| v.public_key == *sender) {
                    let msg = blob2.message();
                    writer.write_all(msg).await.unwrap();
                }
            }
        }
    });

    let f1 = tokio::spawn(async move {
        let mut buf = [0u8; MAX_APP_MESSAGE_LEN];
        loop {
            let n = reader.read(&mut buf).await.unwrap();
            if let Err(e) = tx.send_async(buf[..n].to_vec()).await {
                error!("tx send: {:?}", e);
            }
        }
    });

    let handle = async {
        tokio::select! {
            res1 = rx2 => res1.unwrap(),
            res2 = f1 => res2.unwrap(),
        }
        Ok(())
    };

    Ok(handle)
}

#[tracing::instrument(err, skip_all)]
async fn ingress_iteration(
    rc: &RoutingContext,
    nodes: &[Node],
    my_ip: Ipv4Addr,
    rx_from_tun: &Receiver<Vec<u8>>,
) -> Result<(), Error> {
    select! {
        res = rx_from_tun.recv_async() => {
            let payload = res.context("rx_from_tun.recv_async")?;
            let value = PacketHeaders::from_ip_slice(&payload).context("can't parse the packet")?;
            if let Some(ip) = &value.ip {
                match ip {
                    IpHeader::Version4(header, _ext) => {
                        let destination = Ipv4Addr::from(header.destination);
                        if destination != my_ip {

                            if let Some(pub_key) = nodes.iter()
                            .find(|v| v.addr == destination)
                            .map(|v| v.public_key) {
                                let target =
                                veilid_core::Target::NodeId(pub_key);

                                if let Err(e) = rc.app_message(target.clone(), payload).await
                                {
                                    warn!("err sending: {:?}", e);
                                }
                            }
                        }
                    }
                    IpHeader::Version6(_, _) => (),
                }
            }
        }
    };

    Ok(())
}

#[tracing::instrument(err, skip_all)]
pub async fn handle_ingress(
    rc: RoutingContext,
    nodes: Vec<Node>,
    my_ip: Ipv4Addr,
    rx_from_tun: Receiver<Vec<u8>>,
) -> Result<(), Error> {
    loop {
        if let Err(e) = ingress_iteration(&rc, &nodes, my_ip, &rx_from_tun).await {
            error!("ingress_iteration: {:?}", e);
        }
    }
}
