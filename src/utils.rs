use anyhow::Error;
use tokio::time::{sleep, Duration};
use tracing::debug;
use veilid_core::{AttachmentManager, AttachmentState, VeilidAPI};
use AttachmentState::{
    AttachedGood, AttachedStrong, AttachedWeak, Attaching, Detached, Detaching, FullyAttached,
    OverAttached,
};

#[tracing::instrument(skip_all)]
pub async fn wait_for_attached(attachment_manager: &AttachmentManager) {
    debug!("awaiting attachment");
    loop {
        match attachment_manager.get_attachment_state() {
            Detached | Attaching | Detaching => (),
            AttachedWeak | AttachedGood | AttachedStrong | FullyAttached | OverAttached => break,
        }
        sleep(Duration::from_millis(100)).await;
    }
    debug!("awaiting attachment, done");
}

#[tracing::instrument(skip_all)]
pub async fn wait_for_network_start(api: &VeilidAPI) {
    debug!("awaiting network initialization");
    loop {
        match api.get_state().await {
            Ok(vs) => {
                if vs.network.started && !vs.network.peers.is_empty() {
                    debug!(
                        "awaiting network initialization, done ({} peer(s))",
                        vs.network.peers.len()
                    );
                    break;
                }
            }
            Err(e) => {
                panic!("Getting state failed: {:?}", e);
            }
        }
        sleep(Duration::from_millis(100)).await;
    }
}

#[tracing::instrument(skip_all)]
pub async fn wait_for_public_internet_ready(
    attachment_manager: &AttachmentManager,
) -> Result<(), Error> {
    debug!("awaiting 'public_internet_ready'");
    loop {
        let state = attachment_manager.get_veilid_state();
        if state.public_internet_ready {
            break;
        }
        sleep(Duration::from_secs(5)).await;
    }
    debug!("awaiting 'public_internet_ready', done");
    Ok(())
}
